{%- from slspath + "/map.jinja" import nextcloud with context %}

PHP 7.2 Repository:
  pkgrepo.managed:
    - humanname: PHP-FPM 7.2 Repository, Debian (neeeded for NextCloud)
    - name: deb https://packages.sury.org/php/ stretch main
    - gpgcheck: 1
    - gpgkey: https://packages.sury.org/php/apt.gpg

remove_libapache2-mod-php7.2:
  pkg.purged:
    - name: apache2

install nextcloud dependencies:
  pkg.installed:
    - skip_verify: True
    - pkgs:
      - php7.2-fpm
      - php7.2-curl
      - php7.2-mbstring
      - php7.2-zip
      - php7.2-bz2
      - php7.2-intl
      - php7.2-pgsql
      - php7.2-ldap
      - php7.2-xml
      - php7.2-gd
      - php7.2-json
      - imagemagick
      - php-imagick
      - php-apcu
      - php-redis
    - require:
      - pkgrepo: PHP 7.2 Repository

ensure nextcloud group is present:
  group.present:
    - name: {{ nextcloud.group }}
    - system: True

ensure nextcloud user is present:
  user.present:
    - name: {{ nextcloud.user }}
    - gid_from_name: {{ nextcloud.group }}
    - shell: {{ nextcloud.user_shell }}
    - home: {{ nextcloud.user_home }}
    {%- if nextcloud.get('user_optional_groups') %}
    - optional_groups:
    {%- for group in nextcloud.get('user_optional_groups', {}) %}
      - {{ group }}
    {%- endfor %}
    {%- endif %}
    - require:
      - group: ensure nextcloud group is present

create nextcloud data directory:
  file.directory:
    - name: {{ nextcloud.data }}
    - user: {{ nextcloud.user }}
    - group: {{ nextcloud.group }}
    - mode: 700
    - makedirs: True
    - require:
      - user: {{ nextcloud.user }}
      - group: {{ nextcloud.group }}

download and extract nextcloud:
  archive.extracted:
    - name: {{ nextcloud.install.base }}
    - source: {{ nextcloud.install.mirror }}/nextcloud-{{ nextcloud.install.version }}.{{ nextcloud.install.format }}
    {%- if nextcloud.install.verify %}
    - source_hash: {{ nextcloud.install.mirror }}/nextcloud-{{ nextcloud.install.version }}.{{ nextcloud.install.format }}.{{ nextcloud.install.hash }}
    {%- else %}
    - skip_verify: True
    {%- endif %}
    - user: {{ nextcloud.user }}
    - group: {{ nextcloud.group }}
    - if_missing: {{ nextcloud.install.base }}/nextcloud
    - require:
      - user: {{ nextcloud.user }}
      - group: {{ nextcloud.group }}

setup nextcloud initial configuration and database:
  cmd.run:
    - env:
        DATABASE_PASSWORD: {{ nextcloud.config.dbpassword }}
        DATABASE_USER: {{ nextcloud.config.dbuser }}
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ "maintenance:install" --database {{ nextcloud.config.dbtype }} --database-name "{{ nextcloud.config.dbname }}" --database-user "$DATABASE_USER" --database-pass "$DATABASE_PASSWORD" --database-host "{{ nextcloud.config.dbhost }}" --database-port "{{ nextcloud.config.dbport }}" --admin-user "{{ nextcloud.admin.user }}" --admin-pass "{{ nextcloud.admin.password }}" --data-dir "{{ nextcloud.config.datadirectory }}"'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - onlyif: '! php7.2 {{ nextcloud.install.base }}/nextcloud/occ config:system:get installed'
    - require:
      - archive: download and extract nextcloud