{# Setup LDAP authentication for Nextcloud #}

{% from "nextcloud/map.jinja" import nextcloud with context %}

{%- if nextcloud.ldap.enabled == True %}

enable ldap:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:enable user_ldap'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    # The : is important, as user_ldap will only have a : if it's installed
    - onlyif: '! php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:list | grep user_ldap:'

{%- for ldapconfig, options in nextcloud.ldap.config.iteritems() %}

setup initial ldap config {{ ldapconfig }}:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:create-empty-config'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - unless: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:show-config | grep {{ ldapconfig }}'
    - require:
      - cmd: enable ldap


{%- for key, value in options.items() %}

ldap config {{ldapconfig}} configure {{ key }}:
  cmd.run:
    - name: "php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:set-config '{{ ldapconfig }}' '{{ key }}' '{{ value }}'"
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    # We are grepping for the key name with a space here because some property names are contained
    # inside other property names. e.g. ldapBase is inside ldapBaseGroups
    - unless: "php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:show-config '{{ ldapconfig }}' | grep '{{ key }} ' |  cut -d '|' -f 3 | grep '{{ value }}'"
    - require:
      - cmd: setup initial ldap config {{ ldapconfig }}
    - require_in:
      - cmd: test config {{ ldapconfig }}

{% endfor %}

test config {{ ldapconfig }}:
  cmd.run:
    - name: "php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:test-config '{{ ldapconfig }}'"
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    # All configuration items will be a requirement for this part

enable config {{ ldapconfig }} if disabled:
  cmd.run:
    - name: "php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:set-config '{{ ldapconfig }}' 'ldapConfigurationActive' '1'"
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - unless: "php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:show-config '{{ ldapconfig }}' | grep 'ldapConfigurationActive' | cut -d '|' -f 3 | grep '1'"
    - require:
      - cmd: test config {{ ldapconfig }}

{%- endfor %}

{%- else %}

{%- for ldapconfig, options in nextcloud.ldap.config.iteritems() %}

disable ldap config {{ ldapconfig }} if present:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:delete {{ ldapconfig }}'
    - runas: {{ nextcloud.user }}
    - onlyif: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ ldap:show-config | grep "{{ ldapconfig }}"'
    - require_in:
      - cmd: disable ldap app if present

{%- endfor %}

disable ldap app if present:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:disable user_ldap'
    - runas: {{ nextcloud.user }}
    - onlyif: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:list | grep user_ldap:'

{%- endif %}